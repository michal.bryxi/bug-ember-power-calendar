import DS from 'ember-data';

export default DS.Model.extend({
  birth: DS.attr('date')
});
